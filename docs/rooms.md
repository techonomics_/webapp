# Rooms

Rooms are part of a community.

Rooms are also referred to as "troupes" internally for legacy reasons.

## Room user

### Leave a room

You can leave a room via **Room settings dropdown** -> **Leave this room**

If you have mad skillz, you can also type "/leave" in the chat room (slash command).

![](https://i.imgur.com/Rc4EVnV.png)


## Room admin

### Room creation

Use the "Add a room" button at the bottom of the conversation list in the left menu to start creating a room.

![](https://i.imgur.com/KxJ2Oym.png)

If you aren't an admin of any communities, you will be redirected to the [create community flow](./communities.md#community-creation).

If you want to associate the room with a GitHub repo, start typing in the the repo name,
and you can select the repo from the typeahead dropdown list.
If you type the full name of the repo, it will auto-associate.
If you don't see the repo listed in the typeahead or isn't being associated, see the [FAQ](./faq.md#why-isnt-my-github-organisation-or-repos-appearing)

![](https://i.imgur.com/8tiwwZM.png)

#### Why isn't my GitHub organisation or repos appearing?

See the [FAQ](./faq.md#why-isn-t-my-github-organisation-or-repos-appearing).


### Room security

**Public rooms**

 - A public room can be seen by everyone

**Private rooms**

 - A room connected to a private repo can be accessed by anyone with access to the repo.
 - A private room with no association can only be accessed if they are manually invited to the room.
 - A private room can also be associated with the community and anyone in the community can join the room. If the community was associated with an org, anyone in the org could join for example



### Restrict room to GitHub users

You can restrict a room to GitHub users via **Room settings dropdown** -> **Settings** -> **Only GitHub users are allowed to join this room.** checkbox

![](https://i.imgur.com/ujd8kHE.png) ![](https://i.imgur.com/oOGoEYw.png)


### Rename a room

If you want to rename a room because a GitHub repo was renamed/transferred, see this [FAQ section instead](./faq.md#what-happens-if-i-rename-something-on-GitHub-org-repo) instead.

Currently, there isn't a way to rename a room in the UI. But you can send a message to support@gitter.im with the following info. Make sure to email with the primary email address associated with the GitLab/GitHub/Twitter account tied to your Gitter account.

 - Link to the current Gitter room
 - Desired room name


### Delete a room

If you are a room admin, you can delete a room via **Room settings dropdown** -> **Delete this room**

![](https://i.imgur.com/FqxWgsM.png)
